
PKG_VERSION	:= $(shell awk '/version/ { print $$3 }' setup.cfg)
PKG_TARBALL	:= dist/ovmfctl-$(PKG_VERSION).tar.gz

FW_IMAGE	:= $(wildcard /usr/share/edk2/ovmf/*.fd)
FW_IMAGE	+= $(wildcard /usr/share/edk2/aarch64/*.fd)

default:
	@echo "targets: lint install uninstall clean"

lint pylint:
	pylint $(PYLINT_OPTS) ovmfctl/

.PHONY: dist
dist tarball $(PKG_TARBALL):
	rm -rf dist
	python3 -m build
	twine check dist/*

install:
	python3 -m pip install --user .

uninstall:
	python3 -m pip uninstall ovmfctl

clean:
	rm -rf build ovmfctl.egg-info $(PKG_TARBALL) rpms dist
	rm -rf *~ ovmfctl/*~ ovmfctl/efi/*~
	rm -rf *~ ovmfctl/__pycache__  ovmfctl/efi/__pycache__
